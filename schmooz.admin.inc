<?php

/**
 * @file
 * Administration forms for the schmooz module.
 */

/**
 * Menu callback; Displays the administration settings for schmooz.
 */
function schmooz_admin_settings() {
  $form = array();
  $form['schmooz_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Shortname'),
    '#description' => t('The website shortname that you registered schmooz with. If you registered http://example.schmooz.com, you would enter "example" here.'),
    '#default_value' => variable_get('schmooz_domain', ''),
  );
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 50,
  );
  // Visibility settings.
  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#group' => 'settings',
  );
  $types = node_type_get_types();
  $options = array();
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['visibility']['schmooz_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#description' => t('Apply comments to only the following node types.'),
    '#default_value' => variable_get('schmooz_nodetypes', array()),
    '#options' => $options,
  );
  $form['visibility']['schmooz_location'] = array(
    '#type' => 'select',
    '#title' => t('Location'),
    '#description' => t('Display the schmooz comments in the given location. When "Block" is selected, the comments will appear in the <a href="@schmoozcomments">schmooz Comments block</a>.', array('@schmoozcomments' => url('admin/structure/block'))),
    '#default_value' => variable_get('schmooz_location', 'content_area'),
    '#options' => array(
      'content_area' => t('Content Area'),
      'block' => t('Block'),
    ),
  );
  $form['visibility']['schmooz_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#description' => t('When the comments are displayed in the content area, you can change the position at which they will be shown.'),
    '#default_value' => variable_get('schmooz_weight', 50),
    '#options' => drupal_map_assoc(array(-100, -75, -50, -25, 0, 25, 50, 75, 100)),
    '#states' => array(
      'visible' => array(
        'select[name="schmooz_location"]' => array('value' => 'content_area'),
      ),
    ),
  );
  // Behavior settings.
  $form['behavior'] = array(
    '#type' => 'fieldset',
    '#title' => t('Behavior'),
    '#group' => 'settings',
  );
  $form['behavior']['schmooz_userapikey'] = array(
    '#type' => 'textfield',
    '#title' => t('User API Key'),
    '#description' => t('The API key of the administrator account on schmooz. You can get yours <a href="@key">here</a>.', array('@key' => 'http://schmooz.com/api/get_my_key/')),
    '#default_value' => variable_get('schmooz_userapikey', ''),
  );
  $form['behavior']['schmooz_localization'] = array(
    '#type' => 'checkbox',
    '#title' => t('Localization support'),
    '#description' => t("When enabled, overrides the language set by schmooz with the language provided by the site."),
    '#default_value' => variable_get('schmooz_localization', FALSE),
  );
  $form['behavior']['schmooz_inherit_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Inherit User Credentials'),
    '#description' => t("When enabled and a user is logged in, the schmooz 'Post as Guest' login form will be pre-filled with the user's name and email address."),
    '#default_value' => variable_get('schmooz_inherit_login', TRUE),
  );
  $form['behavior']['schmooz_developer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Testing'),
    '#description' => t('When enabled, uses the <a href="http://docs.schmooz.com/help/2/">schmooz_developer</a> flag to tell schmooz that you are in a testing environment. Threads will not display on the public community page with this set.'),
    '#default_value' => variable_get('schmooz_developer', FALSE),
  );
  // Advanced settings.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#group' => 'settings',
    '#description' => t('Use these settings to configure the more advanced uses of schmooz. You can find more information about these in the <a href="@applications">Applications</a> section of schmooz. To enable some of these features, you will require a <a href="@addons">schmooz Add-on Package</a>.', array(
      '@applications' => 'http://schmooz.com/api/applications/',
      '@addons' => 'http://schmooz.com/addons/',
    )),
  );
  $form['advanced']['schmooz_publickey'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#default_value' => variable_get('schmooz_publickey', ''),
  );
  $form['advanced']['schmooz_secretkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => variable_get('schmooz_secretkey', ''),
  );
  $form['advanced']['schmooz_sso'] = array(
    '#type' => 'checkbox',
    '#title' => t('Single Sign-On'),
    '#description' => t('Provide <a href="@sso">Single Sign-On</a> access to your site.', array(
      '@sso' => 'http://schmooz.com/api/sso/',
    )),
    '#default_value' => variable_get('schmooz_sso', FALSE),
    '#states' => array(
      'visible' => array(
        'input[name="schmooz_publickey"]' => array('empty' => FALSE),
        'input[name="schmooz_secretkey"]' => array('empty' => FALSE),
      ),
    ),
  );
  return system_settings_form($form);
}

/**
 * Menu callback; Automatically closes the window after the user logs in.
 */
function schmooz_closewindow() {
  drupal_add_js('window.close();', 'inline');
  return t('Thank you for logging in. Please close this window, or <a href="@clickhere">click here</a> to continue.', array('@clickhere' => 'javascript:window.close();'));
}
