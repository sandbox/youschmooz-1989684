<?php
/**
 * @file
 * Views integration with the schmooz module.
 */

/**

 * Implements hook_views_data_alter().

 */

function schmooz_views_data_alter(&$data) {

 // Number of schmooz comments made on the given node.

  $data['node']['schmooz_comment_count'] = array(

    'field' => array(

      'title' => t('schmooz Comment Count'),

      'help' => t('The number of schmooz comments made on the post. Note that this will not work in the preview.'),

      'handler' => 'views_handler_field_node_schmooz_comment_count',

    ),

  );

}

/**

 * Field handler to present the number of schmooz comments on a node.

 */

class views_handler_field_node_schmooz_comment_count extends views_handler_field {

  function init(&$view, &$options) {

    parent::init($view, $options);

  }

  function query() {

    // Override parent::query() without altering query.

  }

  /**

   * When rendering the field.

   */

  function render($values) {

    // Ensure schmooz comments are available on the node user has access to edit this node.
    $api_url = 'http://www.youschmooz.com/schmoozapi/';
	
    $node = node_load($values->nid);

    if (user_access('view schmooz comments') && isset($node->schmooz)) {

      // Extract the schmooz values.

      $schmooz = $node->schmooz;

      // Build a renderable array for the link.

      $content = array(

        '#theme' => 'link',

        '#text' => t('Comments'),

        '#path' => $schmooz['identifier'],

        '#options' => array(

          'fragment' => 'schmooz_thread',

          'attributes' => array(

            // Identify the node for schmooz with the unique identifier:

            // http://docs.schmooz.com/developers/universal/#comment-count

            'data-schmooz-identifier' => $schmooz['identifier'],

          ),

          'html' => FALSE,

        ),

      );

      /**

       * This attaches schmooz.js, which will look for the DOM variable

       * schmoozComments which is set below. When found, the schmooz javascript

       * api replaces the html element with the attribute:

       * "data-schmooz-identifier" and replaces the element with the number of

       * comments on the node.

       */
	   
	 $loadJsFiles =  json_decode(file_get_contents($api_url."pluginFiles.json"));
	 
	 $content['#attached'] = array(

        'js' => array(
			  array('data' => $loadJsFiles),

          array(

            'data' => array('schmoozComments' => $schmooz['domain']),

            'type' => 'setting',

          ),

        ),

      );
    
	 return drupal_render($content);

    }

  }

}

