/**
 * @file
 * JavaScript for the schmooz Drupal module.
 */

// The schmooz global variables.
var schmooz_shortname = '';
var schmooz_url = '';
var schmooz_title = '';
var schmooz_identifier = '';
var schmooz_developer = 0;
var schmooz_def_name = '';
var schmooz_def_email = '';
var schmooz_config;

(function ($) {
/**
 * Drupal schmooz behavior.
 */
Drupal.behaviors.schmooz = {
  attach: function (context, settings) {
    $('body').once('schmooz', function() {
      // Load the schmooz comments.
      if (settings.schmooz || false) {
		
		// Setup the global JavaScript variables for schmooz.
        schmooz_shortname = settings.schmooz.domain;
        schmooz_url = settings.schmooz.url;
        schmooz_title = settings.schmooz.title;
        schmooz_identifier = settings.schmooz.identifier;
        schmooz_developer = settings.schmooz.developer || 0;
        schmooz_def_name = settings.schmooz.name || '';
        schmooz_def_email = settings.schmooz.email || '';

        // Language and SSO settings are passed in through schmooz_config().
		
		var id = schmooz_identifier.split('/');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'artical_id',
			name: 'artical_id',
			value:id[1]
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'param_link',
			name: 'param_link',
			value:schmooz_url.replace(/&/gi, "*")
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'post_url',
			name: 'post_url',
			value:schmooz_url.replace(/&/gi, "*")
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'slug',
			name: 'slug',
			value:schmooz_identifier
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'artical_title',
			name: 'artical_title',
			value:schmooz_title
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'shortname',
			name: 'shortname',
			value:schmooz_shortname
		}).appendTo('body');
		
		$('<input>').attr({
			type: 'hidden',
			id: 'container_id',
			name: 'container_id',
			value:'comments'
		}).appendTo('body');
		
		
		$('<input>').attr({
			type: 'hidden',
			id: 'created_date',
			name: 'created_date',
			value:'2012-06-16'
		}).appendTo('body');
		
		var script = document.createElement('script');
		script.setAttribute('type', "text/javascript");
		script.setAttribute('src', "http://www.youschmooz.com/schmoozapi/js/embed.js");
		document.getElementsByTagName('head')[0].appendChild(script); 

      }
	});
  }
};

})(jQuery);